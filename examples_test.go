package textindex_test

import (
	"fmt"

	"gitlab.com/etomyutikos/textindex"
)

func ExampleBetween() {
	fmt.Println(textindex.Between("00z07", "00z09"))
	// Output: 00z08
}

func ExampleBetween_noGap() {
	fmt.Println(textindex.Between("0i0n0r", "0i0n0s"))
	// Output: 0i0n0r0i
}

func ExampleBetween_firstItem() {
	fmt.Println(textindex.Between("", ""))
	// Output: 0i
}

func ExampleBetween_increment() {
	fmt.Println(textindex.Between("0o0p06", ""))
	// Output: 0o0p07
}

func ExampleBetween_incrementAtLimit() {
	fmt.Println(textindex.Between("0h040z", ""))
	// Output: 0h040z0i
}

func ExampleBetween_decrement() {
	fmt.Println(textindex.Between("", "00b070z"))
	// Output: 00b070y
}

func ExampleBetween_decrementAtLimit() {
	fmt.Println(textindex.Between("", "0600301"))
	// Output: 0600300i
}

func ExampleGenerator_Between() {
	var g textindex.Generator
	fmt.Println(g.Between("0f00r03", "0f00r05"))
	// Output: 0f00r04
}

func ExampleGenerator_Between_alternativeCharSet() {
	g := textindex.Generator{
		CharSet: "ABCDEFGHIJKLMNOPQRSTUVWXYZ",
	}
	fmt.Println(g.Between("ANAB", "ANAC"))
	// Output: ANABAN
}
