module gitlab.com/etomyutikos/textindex

go 1.13

require (
	github.com/jawher/mow.cli v1.1.0
	github.com/stretchr/testify v1.5.1
)
