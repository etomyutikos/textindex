package main

import (
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"sort"
	"syscall"
	"time"

	cli "github.com/jawher/mow.cli"

	"gitlab.com/etomyutikos/textindex"
)

func main() {
	app := cli.App("fuzz", "tests randomized indexes looking for errors")

	app.Action = func() {
		quit := make(chan os.Signal)
		signal.Notify(quit,
			os.Interrupt,
			os.Kill,
			syscall.SIGTERM,
		)

		go func() {
			<-quit
			fmt.Println("quitting...")
			os.Exit(1)
		}()

		testTicker := time.NewTicker(100 * time.Millisecond)
		statusTicker := time.NewTicker(30 * time.Second)

		start := time.Now()
		var passes int
		var failures int

		indexes := []string{textindex.Between("", "")}
		rand.Seed(time.Now().UnixNano())
		for {
			select {
			case <-statusTicker.C:
				fmt.Printf("duration: %s; passes: %d; failures: %d\n", time.Since(start), passes, failures)

			case <-testTicker.C:
				var previous string
				var next string

				x := rand.Intn(len(indexes))
				if x == 0 {
					if rand.Intn(2) == 0 {
						previous = indexes[0]
						if len(indexes) > 1 {
							next = indexes[1]
						}
					} else {
						next = indexes[0]
					}
				} else {
					previous = indexes[x]
					if x < len(indexes)-1 {
						next = indexes[x+1]
					}
				}

				i := textindex.Between(previous, next)

				var failure bool
				if previous == "" {
					failure = i >= next
				}
				if next == "" {
					failure = i <= previous
				}
				if previous != "" && next != "" {
					failure = i <= previous || i >= next
				}

				if failure {
					failures++
					fmt.Printf("previous: %q; next: %q; index: %q\n", previous, next, i)
				} else {
					indexes = append(indexes, i)
					sort.Strings(indexes)
				}

				passes++
			}
		}
	}

	app.Run(os.Args)
}
