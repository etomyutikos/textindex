package main

import (
	"fmt"
	"os"

	cli "github.com/jawher/mow.cli"
	"gitlab.com/etomyutikos/textindex"
)

func stringOrEmpty(s *string) string {
	if s == nil {
		return ""
	}
	return *s
}

var version string

func main() {
	app := cli.App("textindex", "generates text indexes for lexographic sorting")
	app.Version("version", version)

	var (
		charSet  = app.StringOpt("c charset", "", "use a custom character set to generate the index")
		previous = app.StringOpt("p previous", "", "index before the one being generated")
		next     = app.StringOpt("n next", "", "index after the one being generated")
	)
	app.Spec = "[-c] [-p] [-n]"

	app.Action = func() {
		var g textindex.Generator
		if stringOrEmpty(charSet) != "" {
			g.CharSet = *charSet
		}

		fmt.Fprintf(os.Stdout, g.Between(stringOrEmpty(previous), stringOrEmpty(next))+"\n")
	}

	app.Run(os.Args)
}
