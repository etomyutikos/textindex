package textindex

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCase struct {
	name     string
	previous string
	next     string
	expected string
}

func makeTestCases(cs string) []testCase {
	first := string(cs[0])
	half := len(cs) / 2
	lead := first + string(cs[half])

	return []testCase{
		{
			"New",
			"",
			"",
			lead, // 0i
		},
		{
			"Before",
			"",
			lead,                       // 0i
			first + string(cs[half-1]), // 0h
		},
		{
			"After",
			lead, // 0i
			"",
			first + string(cs[half+1]), // 0j
		},
		{
			"Gap",
			first + string(cs[half-2]), // 0g
			lead,                       // 0i
			first + string(cs[half-1]), // 0h
		},
		{
			"BeforeNoGap",
			first + string(cs[half-1]), // 0h
			lead,                       // 0i
			first + string(cs[half-1]) + first + string(cs[half]), // 0h0i
		},
		{
			"BeforeLowest",
			"",
			first + string(cs[1]),            // 01
			first + first + string(cs[half]), // 00i
		},
		{
			"AfterHighest",
			first + string(cs[len(cs)-1]), // 0z
			"",
			first + string(cs[len(cs)-1]) + first + string(cs[half]), // 0z0i
		},
		{
			"IncrementPrevious",
			lead + lead,                       // 0i0i
			first + string(cs[half+1]),        // 0j
			lead + first + string(cs[half+1]), // 0i0j
		},
	}
}

func TestBetween(t *testing.T) {
	t.Parallel()

	for _, test := range makeTestCases(defaultCharSet) {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()

			actual := Between(test.previous, test.next)
			assert.Equal(t, test.expected, actual)
		})
	}
}

func TestGenerator(t *testing.T) {
	t.Parallel()

	t.Run("DefaultCharSet", func(t *testing.T) {
		t.Parallel()

		for _, test := range makeTestCases(defaultCharSet) {
			test := test
			t.Run(test.name, func(t *testing.T) {
				t.Parallel()

				actual := Generator{}.Between(test.previous, test.next)
				assert.Equal(t, test.expected, actual)
			})
		}
	})

	t.Run("AlternateCharSet", func(t *testing.T) {
		t.Parallel()

		cs := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		for _, test := range makeTestCases(cs) {
			test := test
			t.Run(test.name, func(t *testing.T) {
				t.Parallel()

				actual := Generator{CharSet: cs}.Between(test.previous, test.next)
				assert.Equal(t, test.expected, actual)
			})
		}
	})
}
