VERSION := $(shell git describe --tags --always)
OUT := ./build/textindex

.PHONY: build
build:
	go build -ldflags '-X main.version=$(VERSION)' -o $(OUT) ./cmd/textindex

.PHONY: install
install:
	go install -ldflags '-X main.version=$(VERSION)' ./cmd/textindex

TESTCMD := go test
ifneq ($(shell which gotest),'')
	TESTCMD=$(shell which gotest)
endif

.PHONY: testcover
testcover:
	$(TESTCMD) -race -timeout 1s -coverprofile=coverage.out ./...
	go tool cover -html=coverage.out -o coverage.html

fuzz:
	go run ./run/fuzz
