# textindex
[![pipeline status](https://gitlab.com/etomyutikos/textindex/badges/master/pipeline.svg)](https://gitlab.com/etomyutikos/textindex/-/commits/master)
[![coverage report](https://gitlab.com/etomyutikos/textindex/badges/master/coverage.svg)](https://gitlab.com/etomyutikos/textindex/-/commits/master)
[![godoc](http://img.shields.io/badge/godoc-reference-5272B4.svg?style=flat-square)](https://godoc.org/gitlab.com/etomyutikos/textindex)

Package textindex implements a method for generating string indexes for sortable lists.

Utilizing the (theoretically) infinite precision of strings, we can create indexes that allow for arbitrary list ordering without having to update all subsequent items in the list.

## CLI
A command line interface exists in `cmd/textindex`, to generate indexes via script.

Installation should be as simple as...
```bash
go install gitlab.com/etomyutikos/textindex/cmd/textindex
```

Usage can be seen with...
```bash
textindex --help
```
