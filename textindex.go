// Package textindex implements a method for generating string indexes for
// sortable lists.
//
// Utilizing the (theoretically) infinite precision of strings, we can create
// indexes that allow for arbitrary list ordering without having to update all
// subsequent items in the list.
package textindex

import (
	"strings"
)

type charSet string

func (s charSet) at(x int) string {
	return string(s[x])
}

func (s charSet) middle() string {
	return s.at(len(s) / 2)
}

func (s charSet) lead() string {
	return s.at(0) + s.middle()
}

func (s charSet) index(t string) int {
	return strings.Index(string(s), t)
}

func (s charSet) last() string {
	return s.at(len(s) - 1)
}

type index string

func (i index) prefix() string {
	return string(i[:len(i)-1])
}

func (i index) last() string {
	return string(i[len(i)-1])
}

func (i index) String() string {
	return string(i)
}

func decrement(cs charSet, i index) string {
	last := i.last()
	if last == cs.at(1) {
		return i.prefix() + cs.lead()
	}

	x := cs.index(last)
	return i.prefix() + cs.at(x-1)
}

func increment(cs charSet, i index) string {
	last := i.last()
	if last == cs.last() {
		return i.String() + cs.lead()
	}

	x := cs.index(last)
	return i.prefix() + cs.at(x+1)
}

func between(cs charSet, previous, next index) string {
	// TODO(Erik): sanity checks?
	if previous == "" && next == "" {
		return cs.lead()
	}
	if previous == "" {
		return decrement(cs, next)
	}
	if next == "" {
		return increment(cs, previous)
	}

	i := decrement(cs, next)
	if i > previous.String() {
		return i
	}

	i = i + cs.lead()
	if i > previous.String() {
		return i
	}

	return increment(cs, previous)
}

// Generator encapsulates a character set for configurability and reusability.
// It also enables use as a mockable dependency.
type Generator struct {
	CharSet string
}

const defaultCharSet = "0123456789abcdefghijklmnopqrstuvwxyz"

// Between generates a new index between the given values using the Generator.
// If no character set has been set, the default character set will be used.
func (g Generator) Between(previous, next string) string {
	if g.CharSet == "" {
		g.CharSet = defaultCharSet
	}
	return between(charSet(g.CharSet), index(previous), index(next))
}

// Between generates a new index between the given values using the default
// character set.
func Between(previous, next string) string {
	return between(defaultCharSet, index(previous), index(next))
}
